package com.example.restclient.service;


import com.example.restclient.model.RequestForm;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service(value = "EmailService")
public class SendEmail {

    private final JavaMailSender mailSender;

    public SendEmail(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendAcceptEmail(RequestForm requestForm)
            throws MessagingException, UnsupportedEncodingException {
        String toAddress = requestForm.getEmail();
        String fromAddress = "devopsgct@gmail.com";
        String senderName = "devopsGCT";
        String subject = "Request Status";
        String content = "Dear [[name]],<br>"
                + "Your Request has been <b>ACCEPTED</b><br>"
                + " your holiday start from <b>[[dateDebut]]</b> to <b>[[dateFin]]</b> <br>"
                + "Enjoy your vacation,<br>"
                + "devopsGCT.";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);

        content = content.replace("[[name]]", requestForm.getMatricule());
        content = content.replace("[[dateDebut]]", requestForm.getDateDebut().toString());
        content = content.replace("[[dateFin]]", requestForm.getDateFin().toString());

        helper.setText(content, true);
        mailSender.send(message);

    }

    public void sendRejectEmail(RequestForm requestForm)
            throws MessagingException, UnsupportedEncodingException {
        String toAddress = requestForm.getEmail();
        String fromAddress = "devopsgct@gmail.com";
        String senderName = "devopsGCT";
        String subject = "Request Status";
        String content = "Dear [[name]],<br>"
                + "we are sorry to inform you that your Request has been <b>REJECTED</b><br>"
                + "Thank you,<br>"
                + "devopsGCT.";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);

        content = content.replace("[[name]]", requestForm.getMatricule());

        helper.setText(content, true);

        mailSender.send(message);

    }

}

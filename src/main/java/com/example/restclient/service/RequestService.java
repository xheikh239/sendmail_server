package com.example.restclient.service;

import com.example.restclient.model.RequestForm;
import com.example.restclient.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service(value = "requestService")
@Transactional
public class RequestService {
    private final RequestRepository requestRepository;

    @Autowired
    public RequestService(RequestRepository requestRepository){
        this.requestRepository = requestRepository;
    }

    public void updateRequest(RequestForm request) {
        requestRepository.save(request);
    }

    public RequestForm fRequestById(Long id) {
        return requestRepository.findByidsql(id);
    }


}

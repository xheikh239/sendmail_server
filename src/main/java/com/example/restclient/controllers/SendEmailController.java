package com.example.restclient.controllers;

import com.example.restclient.model.RequestForm;
import com.example.restclient.service.RequestService;
import com.example.restclient.service.SendEmail;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

@RestController
@CrossOrigin("http://localhost:4200")
public class SendEmailController {

    private final RequestService requestService;
    private final SendEmail sendEmail;

    public SendEmailController(RequestService requestService, SendEmail sendEmail) {
        this.requestService = requestService;
        this.sendEmail = sendEmail;
    }

    @GetMapping(value="/sendmail/accept/{id}")
    public ResponseEntity<RequestForm> acceptRequest(@PathVariable(value = "id") long id)
            throws MessagingException, UnsupportedEncodingException {
    RequestForm request = requestService.fRequestById(id);
            request.setAccept(true);
            request.setActive(false);
            sendEmail.sendAcceptEmail(request);
        requestService.updateRequest(request);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @GetMapping(value="/sendmail/reject/{id}")
    public ResponseEntity<RequestForm> rejectRequest(@PathVariable(value = "id") long id)
            throws MessagingException, UnsupportedEncodingException {
        RequestForm request = requestService.fRequestById(id);
        request.setAccept(false);
        request.setActive(false);//set to false
        sendEmail.sendRejectEmail(request);
        requestService.updateRequest(request);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

}

package com.example.restclient.repository;

import com.example.restclient.model.RequestForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository(value = "requestRepository")
public interface RequestRepository extends JpaRepository<RequestForm, Serializable> {

    @Query(nativeQuery = true, value = "select * from demande_conge where id =?")
    RequestForm findByidsql(Long id);
}

